import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

void main() {
  runApp(
    const MaterialApp(
      title: 'Weather API Example',
      home: MyHomePage(),
    ),
  );
}

class Weather extends StatelessWidget {
  final Map<String, dynamic> data;
  const Weather(this.data, {super.key});

  @override
  Widget build(BuildContext context) {
    double temp = data['main']['temp'];
    return Text(
      '${temp.toStringAsFixed(1)} °C',
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  MyHomePageState createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> {
  late Future<http.Response> _response;

  @override
  void initState() {
    super.initState();
    _refresh();
  }

  void _refresh() {
    setState(() {
      _response = http.get(Uri.parse(
          "http://api.openweathermap.org/data/2.5/forecast?q=San+Francisco&units=metric&APPID=14cc828bff4e71286219858975c3e89a"));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Weather API Example"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _refresh,
        child: const Icon(Icons.refresh),
      ),
      body: Center(
        child: FutureBuilder(
          future: _response,
          builder:
              (BuildContext context, AsyncSnapshot<http.Response> response) {
            if (!response.hasData) {
              return const Text('Loading...');
            } else if (response.data?.statusCode != 200) {
              return const Text('Could not connect to weather service.');
            } else {
              Map<String, dynamic> json = jsonDecode(response.data!.body);
              if (json['cod'] == 200) {
                return Weather(json);
              } else {
                return Text('Weather service error: $json.');
              }
            }
          },
        ),
      ),
    );
  }
}
